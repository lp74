use std::usize;

use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc;
use std::thread;

const SPACE_SIZE: usize = 21;
const NOF_DIM: usize = 3;
const DEFAULT_FACE_SIZE: usize = 64;
const NOF_THREADS: usize = 4;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Axis {
    X,
    Y,
    Z,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Ori {
    Top,
    Back,
    Side,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Pos {
    E(Edge),
    F(Face),
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Change {
    None,
    Current,
    Size(Edge, usize),
    Space,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum PosKind {
    Edge,
    Face,
}

pub type Coord = (usize, usize, usize);
pub type Edge = (Coord, Axis);
pub type Face = (Coord, Ori);
pub type FaceColor = (f32, f32, f32);
pub type Surface = [Option<FaceColor>; NOF_DIM];

pub struct Space {
    pub size: [[usize; SPACE_SIZE]; NOF_DIM],
    pub faces: [[[Option<Surface>; SPACE_SIZE]; SPACE_SIZE]; SPACE_SIZE],
    pub current: Pos,
    pub change: Change,
}

impl Space {
    fn new() -> Space {
        let mid = SPACE_SIZE / 2;
        let current_node = (mid, mid, mid);
        let mut tmp = Space {
            size: [[DEFAULT_FACE_SIZE; SPACE_SIZE]; NOF_DIM],
            faces: [[[None; SPACE_SIZE]; SPACE_SIZE]; SPACE_SIZE],
            current: Pos::E((current_node, Axis::X)),
            change: Change::None,
        };
        tmp.add_empty_node(current_node);
        tmp.add_face((current_node, Ori::Top));
        tmp
    }

    #[inline]
    pub fn axis_idx(a: Axis) -> usize {
        match a {
            Axis::X => 0,
            Axis::Y => 1,
            Axis::Z => 2,
        }
    }

    #[inline]
    pub fn ori_idx(o: Ori) -> usize {
        match o {
            Ori::Top => 0,
            Ori::Side => 1,
            Ori::Back => 2,
        }
    }

    fn adj_faces(((x, y, z), a): Edge) -> Vec<Face> {
        let mut faces = Vec::new();
        match a {
            Axis::X => {
                faces.push(((x, y, z), Ori::Top));
                faces.push(((x, y, z), Ori::Back));
                if y < (SPACE_SIZE - 1) {
                    faces.push(((x, y+1, z), Ori::Top));
                }
                if z > 0 {
                   faces.push(((x, y, z-1), Ori::Back));
                }
            },
            Axis::Y => {
                faces.push(((x, y, z), Ori::Top));
                faces.push(((x, y, z), Ori::Side));
                if x > 0 {
                    faces.push(((x-1, y, z), Ori::Top));
                }
                if z > 0 {
                    faces.push(((x, y, z-1), Ori::Side));
                }
            },
            Axis::Z => {
                faces.push(((x, y, z), Ori::Back));
                faces.push(((x, y, z), Ori::Side));
                if x > 0 {
                    faces.push(((x-1, y, z), Ori::Back));
                }
                if y < (SPACE_SIZE - 1) {
                    faces.push(((x, y+1, z), Ori::Side));
                }
            },
        }
        faces
    }

    #[inline]
    fn is_node(&self, (x, y, z): Coord) -> bool {
        if x < SPACE_SIZE
            && y < SPACE_SIZE
            && z < SPACE_SIZE {
            if let Some(_) = self.faces[x][y][z] {
                true
            } else {
                false
            }
        } else {
            false
        }
    }

    #[inline]
    fn is_face(&self, ((x, y, z), o): Face) -> bool {
        self.is_node((x, y, z))
            && match self.faces[x][y][z] {
            Some(faces) => faces[Space::ori_idx(o)] != None,
            None => false,
        }
    }

    #[inline]
    pub fn current_node_coord(&self) -> Coord {
        match self.current {
            Pos::E((n, _)) => n,
            Pos::F((n, _)) => n,
        }
    }

    pub fn row_size_before_current(&self, a: Axis) -> usize {
        let (xcur, ycur, zcur) = self.current_node_coord();
        let mut res = 0;
        match a {
            Axis::X => {
                for x in 0..xcur {
                    res += self.size[Space::axis_idx(Axis::X)][x];
                }
                res += self.size[Space::axis_idx(Axis::X)][xcur] / 2;
            },
            Axis::Y => {
                for y in 0..ycur {
                    res += self.size[Space::axis_idx(Axis::Y)][y];
                }
                res += self.size[Space::axis_idx(Axis::Y)][ycur] / 2;
            },
            Axis::Z => {
                for z in 0..zcur {
                    res += self.size[Space::axis_idx(Axis::Z)][z];
                }
                res += self.size[Space::axis_idx(Axis::Z)][zcur] / 2;
            },
        }
        res
    }

    #[inline]
    pub fn row_size(&self, a: Axis) -> usize {
        self.size[Space::axis_idx(a)].iter()
            .fold(0, |row, &s| row + s)
    }

    #[inline]
    pub fn get_edge_size(&self, ((x, y, z), a): Edge) -> usize {
        self.size[Space::axis_idx(a)][match a {
            Axis::X => x,
            Axis::Y => y,
            Axis::Z => z,
        }]
    }

    pub fn set_current_face_color_if_any(&mut self, c: FaceColor) {
        match self.current {
            Pos::E(_) => panic!("current is not a face"),
            Pos::F(((x, y, z), o)) => match self.faces[x][y][z] {
                Some(ref mut f) => f[Space::ori_idx(o)] = Some(c),
                None => panic!("no face here"),
            },
        }
    }

    fn edge_size_no_change(&mut self, ((x, y, z), a): Edge, new_size: usize) {
        let size = &mut self.size[Space::axis_idx(a)][match a {
            Axis::X => x,
            Axis::Y => y,
            Axis::Z => z,
        }];
        if *size != new_size {
            *size = new_size;
        }
    }

    #[inline]
    pub fn edge_size(&mut self, e: Edge, new_size: usize) {
        self.edge_size_no_change(e, new_size);
        self.change = Change::Size(e, new_size);
    }

    pub fn inc_size(&mut self) {
        match self.current {
            Pos::E(e) => {
                let size = self.get_edge_size(e);
                if size < usize::MAX {
                    self.edge_size(e, size + 1);
                }
            },
            Pos::F(_) => panic!("resize on a face"),
        }
    }

    pub fn dec_size(&mut self) {
        match self.current {
            Pos::E(e) => {
                let size = self.get_edge_size(e);
                if size > 0 {
                    self.edge_size(e, size - 1);
                }
            },
            Pos::F(_) => panic!("resize on a face"),
        }
    }

    fn face_edges(&self, ((x, y, z), o): Face) -> Vec<Edge> {
        let mut edges = Vec::new();
        match o {
            Ori::Top => {
                edges.push(((x, y, z), Axis::X));
                edges.push(((x, y, z), Axis::Y));
                if x < SPACE_SIZE-1 {
                    edges.push(((x+1, y, z), Axis::Y));
                }
                if y > 0 {
                    edges.push(((x, y-1, z), Axis::X));
                }
            },
            Ori::Back => {
                edges.push(((x, y, z), Axis::X));
                edges.push(((x, y, z), Axis::Z));
                if x < SPACE_SIZE-1 {
                    edges.push(((x+1, y, z), Axis::Z));
                }
                if z < SPACE_SIZE-1 {
                    edges.push(((x, y, z+1), Axis::X));
                }
            },
            Ori::Side => {
                edges.push(((x, y, z), Axis::Y));
                edges.push(((x, y, z), Axis::Z));
                if y > 0 {
                    edges.push(((x, y-1, z), Axis::Z));
                }
                if z < SPACE_SIZE-1 {
                    edges.push(((x, y, z+1), Axis::Y));
                }
            },
        }
        edges
    }

    #[inline]
    fn add_empty_node(&mut self, (x, y, z): Coord) {
        assert!(!self.is_node((x, y, z)));
        self.faces[x][y][z] = Some([None; NOF_DIM]);
        self.change = Change::Space;
    }

    fn adj_available_faces(&self, e: Edge) -> Vec<Face> {
        Space::adj_faces(e).iter()
            .filter_map(|&adj_f| if self.is_face(adj_f) {
                None
            } else {
                Some(adj_f)
            }).collect::<Vec<_>>()
    }

    fn adj_existing_faces(&self, e: Edge) -> Vec<Face> {
        Space::adj_faces(e).iter()
            .filter_map(|&adj_f| if self.is_face(adj_f) {
                Some(adj_f)
            } else {
                None
            }).collect::<Vec<_>>()
    }

    pub fn dec_pos(&mut self) {
        let tmp = match self.current {
            Pos::E(((mut x, mut y, mut z), a)) => {
                match a {
                    Axis::X => x -= 1,
                    Axis::Y => y -= 1,
                    Axis::Z => z -= 1,
                };
                (x, y, z)
            },
            Pos::F(((mut x, mut y, mut z), o)) => {
                match o {
                    Ori::Top => z -= 1,
                    Ori::Back => y -= 1,
                    Ori::Side => x -= 1,
                };
                (x, y, z)
            },
        };
        if self.is_node(tmp) {
            self.change = Change::Current;
            match self.current {
                Pos::E((ref mut n, _)) => *n = tmp,
                Pos::F((ref mut n, _)) => *n = tmp,
            }
        };
    }

    pub fn inc_pos(&mut self) {
        let tmp = match self.current {
            Pos::E(((mut x, mut y, mut z), a)) => {
                match a {
                    Axis::X => x += 1,
                    Axis::Y => y += 1,
                    Axis::Z => z += 1,
                };
                (x, y, z)
            },
            Pos::F(((mut x, mut y, mut z), o)) => {
                match o {
                    Ori::Top => z += 1,
                    Ori::Back => y += 1,
                    Ori::Side => x += 1,
                };
                (x, y, z)
            },
        };
        if self.is_node(tmp) {
            self.change = Change::Current;
            match self.current {
                Pos::E((ref mut n, _)) => *n = tmp,
                Pos::F((ref mut n, _)) => *n = tmp,
            }
        };
    }

    pub fn inc_pos2(&mut self) {
        let tmp = match self.current {
            Pos::E(_) => panic!(),
            Pos::F(((mut x, mut y, mut z), o)) => {
                match o {
                    Ori::Top => y += 1,
                    Ori::Back => x += 1,
                    Ori::Side => z += 1,
                };
                (x, y, z)
            },
        };
        if self.is_node(tmp) {
            self.change = Change::Current;
            match self.current {
                Pos::E(_) => {},
                Pos::F((ref mut n, _)) => *n = tmp,
            }
        };
    }

    pub fn dec_pos2(&mut self) {
        let tmp = match self.current {
            Pos::E(_) => panic!(),
            Pos::F(((mut x, mut y, mut z), o)) => {
                match o {
                    Ori::Top => y -= 1,
                    Ori::Back => x -= 1,
                    Ori::Side => z -= 1,
                };
                (x, y, z)
            },
        };
        if self.is_node(tmp) {
            self.change = Change::Current;
            match self.current {
                Pos::E(_) => {},
                Pos::F((ref mut n, _)) => *n = tmp,
            }
        };
    }

    pub fn inc_pos3(&mut self) {
        let tmp = match self.current {
            Pos::E(_) => panic!(),
            Pos::F(((mut x, mut y, mut z), o)) => {
                match o {
                    Ori::Top => x += 1,
                    Ori::Back => z += 1,
                    Ori::Side => y += 1,
                };
                (x, y, z)
            },
        };
        if self.is_node(tmp) {
            self.change = Change::Current;
            match self.current {
                Pos::E(_) => {},
                Pos::F((ref mut n, _)) => *n = tmp,
            }
        };
    }

    pub fn dec_pos3(&mut self) {
        let tmp = match self.current {
            Pos::E(_) => panic!(),
            Pos::F(((mut x, mut y, mut z), o)) => {
                match o {
                    Ori::Top => x -= 1,
                    Ori::Back => z -= 1,
                    Ori::Side => y -= 1,
                };
                (x, y, z)
            },
        };
        if self.is_node(tmp) {
            self.change = Change::Current;
            match self.current {
                Pos::E(_) => {},
                Pos::F((ref mut n, _)) => *n = tmp,
            }
        };
    }

    pub fn rotate(&mut self) {
        self.change = Change::Current;
        match self.current {
            Pos::E((_, ref mut a)) => match *a {
                Axis::X => *a = Axis::Y,
                Axis::Y => *a = Axis::Z,
                Axis::Z => *a = Axis::X,
            },
            Pos::F((_, ref mut o)) => match *o {
                Ori::Top => *o = Ori::Back,
                Ori::Back => *o = Ori::Side,
                Ori::Side => *o = Ori::Top,
            },
        }
    }

    pub fn set_pos_kind(&mut self, pk: PosKind) {
        match pk {
            PosKind::Face => if let Pos::E((c, a)) = self.current {
                self.change = Change::Current;
                self.current = Pos::F((c, match a {
                    Axis::X => Ori::Side,
                    Axis::Y => Ori::Back,
                    Axis::Z => Ori::Top,
                }));
            },
            PosKind::Edge => if let Pos::F((c, o)) = self.current {
                self.change = Change::Current;
                self.current = Pos::E((c, match o {
                    Ori::Side => Axis::X,
                    Ori::Back => Axis::Y,
                    Ori::Top => Axis::Z,
                }));
            },
        };
    }

    fn all_nodes_coord(&self) -> Vec<Coord> {
        let mut nodes = Vec::new();
        for x in 0..self.faces.len() {
            for y in 0..self.faces[x].len() {
                for z in 0..self.faces[x][y].len() {
                    if self.is_node((x, y, z)) {
                        nodes.push((x, y, z));
                    }
                }
            }
        }
        nodes
    }

    fn connecting_edge(&self, f1: Face, f2: Face) -> Option<Edge> {
        for e1 in self.face_edges(f1) {
            for e2 in self.face_edges(f2) {
                if e1 == e2 {
                    return Some(e1);
                }
            }
        }
        None
    }

    fn all_faces(&self) -> Vec<Face> {
        let mut faces = Vec::new();
        for n in self.all_nodes_coord() {
            for &o in [Ori::Top, Ori::Side, Ori::Back].iter() {
                if self.is_face((n, o)) {
                    faces.push((n, o));
                }
            }
        }
        faces
    }

    fn faces_map(&self) -> Vec<(Face, Vec<Face>)> {
        let mut map = Vec::new();
        for f in self.all_faces() {
            let mut adj_faces = Vec::new();
            for &e in self.face_edges(f).iter() {
                for &adj_f in self.adj_existing_faces(e).iter() {
                    if f != adj_f {
                        adj_faces.push(adj_f);
                    }
                }
            }
            map.push((f, adj_faces));
        }
        map
    }

    #[inline]
    fn face_dot_label_fmt(((x, y, z), o): Face, s: &str) -> String {
        format!("f{0}_{1}_{2}_{3} [label=\"{0} {1} {2} {3}\" {4}]",
                x, y, z, Space::ori_idx(o), s)
    }

    #[inline]
    fn face_dot_fmt(((x, y, z), o): Face) -> String {
        format!("f{}_{}_{}_{}", x, y, z, Space::ori_idx(o))
    }

    fn dot_face_pairs(&self) -> Vec<(Face, Face)> {
        let mut pairs = Vec::new();
        {
            let fmap = self.faces_map();
            let (f_tx, f_rx): (Sender<(Face, Face)>, Receiver<(Face, Face)>) = mpsc::channel();
            let mut nof_pairs = 0;
            for map_chunk in fmap.chunks((fmap.len() / NOF_THREADS) + 1) {
                let thread_f_tx = f_tx.clone();
                let thread_mc = map_chunk.iter().map(|x| {
                    nof_pairs += x.1.len();
                    x.clone()
                }).collect::<Vec<(Face, Vec<Face>)>>();
                thread::spawn(move || {
                    for (f, adj_fs) in thread_mc {
                        for adj_f in adj_fs {
                            thread_f_tx.send((f, adj_f)).unwrap();
                        }
                    }
                });
            }
            for _ in 0..nof_pairs {
                pairs.push(f_rx.recv().unwrap());
            }
        }
        let mut i = 0;
        pairs.iter().filter_map(|&(a1, b1)| {
            i += 1;
            if !pairs.iter().take(i-1)
                  .any(|&(b2, a2)| a1 == a2 && b1 == b2) {
                Some((a1, b1))
            } else {
                None
            }
        }).collect::<Vec<(Face, Face)>>()
    }

    fn dot_face_decls(&self) -> String {
        let mut decls = String::new();
        {
            let faces = self.all_faces();
            let (s_tx, s_rx): (Sender<String>, Receiver<String>) = mpsc::channel();
            for faces_chunk in faces.chunks((faces.len() / NOF_THREADS) + 1) {
                let thread_s_tx = s_tx.clone();
                let thread_fs = faces_chunk.iter().map(|x| x.clone()).collect::<Vec<Face>>();
                let current = self.current;
                thread::spawn(move || {
                    for f in thread_fs {
                        thread_s_tx.send(format!("    {};\n", Space::face_dot_label_fmt(f,
                            if Pos::F(f) == current { "color=red" } else { "" })))
                            .unwrap();
                    }
                });
            }
            for _ in 0..faces.len() {
                decls.push_str(&s_rx.recv().unwrap());
            }
        }
        decls
    }

    pub fn dot_graph_code(&self) -> String {
        let mut code = String::from("graph {\n");
        code.push_str(&self.dot_face_decls());
        for (a, b) in self.dot_face_pairs() {
            code.push_str(&format!("    {} -- {}{};\n",
                                   Space::face_dot_fmt(a),
                                   Space::face_dot_fmt(b),
                                   if let Some(e) = self.connecting_edge(a, b) {
                                       if Pos::E(e) == self.current {
                                           " [color=red]"
                                       } else { "" }
                                   } else { "" }));
        }
        code.push_str("}");
        code
    }

    #[inline]
    fn add_empty_node_if_none(&mut self, n: Coord) {
        if !self.is_node(n) {
            self.add_empty_node(n);
        }
    }

    fn set_face(&mut self, ((x, y, z), o): Face, v: Option<FaceColor>) {
        match self.faces[x][y][z] {
            Some(ref mut f) => {
                if f[Space::ori_idx(o)] != v {
                    self.change = Change::Space;
                }
                f[Space::ori_idx(o)] = v;
            },
            None => panic!("there is no node here"),
        }
    }

    fn face_adj_available_faces(&self, f: Face) -> Vec<Face> {
        let mut faces = Vec::new();
        for e in self.face_edges(f) {
            for adj_f in self.adj_available_faces(e) {
                if f != adj_f {
                    faces.push(adj_f);
                }
            }
        }
        faces
    }

    fn suround_with_nodes(&mut self, (n, o): Face) {
        assert!(self.is_node(n));
        for (nf, _) in self.face_adj_available_faces((n, o)) {
            self.add_empty_node_if_none(nf);
        }
    }

    #[inline]
    pub fn add_face(&mut self, f: Face) {
        self.suround_with_nodes(f);
        self.set_face(f, Some((1.0, 1.0, 1.0)));
    }

    #[inline]
    pub fn remove_face(&mut self, f: Face) {
        assert!(self.is_face(f));
        self.set_face(f, None);
    }

    pub fn set_current_face_if_none(&mut self) {
        match self.current {
            Pos::F(f) => if !self.is_face(f) { self.add_face(f) },
            Pos::E(_) => {},
        }
    }
}

impl Clone for Space {
    fn clone(&self) -> Space {
        Space {
            size: self.size,
            faces: self.faces,
            current: self.current,
            change: self.change,
        }
    }
}

#[derive(Clone)]
enum Save {
    Space(Space),
    Current(Pos),
    Size(Edge, usize),
}

pub struct History {
    saves: Vec<Save>,
    id: usize,
    pub change: bool,
}

impl History {
    fn new() -> History {
        History {
            saves: Vec::new(),
            id: 0,
            change: false,
        }
    }

    fn add(&mut self, s: Save) {
        if self.saves.len() > self.id {
            while self.saves.len() > self.id {
                self.saves.pop();
            }
        }
        self.saves.push(s);
        self.id += 1;
    }

    pub fn has_prev(&self) -> bool {
        self.id > 1
    }

    pub fn has_next(&self) -> bool {
        self.saves.len() > self.id
    }

    fn prev(&mut self) -> Save {
        assert!(self.has_prev());
        self.id -= 1;
        self.change = true;
        self.saves[self.id-1].clone()
    }

    fn next(&mut self) -> Save {
        assert!(self.has_next());
        self.id += 1;
        self.change = true;
        self.saves[self.id-1].clone()
    }

    fn current(&self) -> Option<&Save> {
        if self.id == 0 {
            None
        } else {
            Some(&self.saves[self.id-1])
        }
    }

    fn replace_current(&mut self, s: Save) {
        assert!(self.id != 0);
        self.saves[self.id-1] = s;
    }
}

pub struct Data {
    pub space: Space,
    pub history: History,
}

impl Data {
    pub fn new() -> Data {
        Data { space: Space::new(), history: History::new() }
    }

    fn save_to_add(&self) -> Option<Save> {
        match self.space.change {
            Change::Space => {
                let mut space = self.space.clone();
                space.change = Change::None;
                Some(Save::Space(space))
            }
            Change::Current => Some(Save::Current(self.space.current)),
            Change::Size(e, s) => Some(Save::Size(e, s)),
            Change::None => None,
        }
    }

    pub fn save_if_changed(&mut self) {
        match self.save_to_add() {
            Some(Save::Size(e, s)) =>
                match self.history.current() {
                    Some(&Save::Size(e, _)) =>
                        self.history.replace_current(Save::Size(e, s)),
                    _ => self.history.add(Save::Size(e, s)),
                },
            Some(s) => self.history.add(s),
            None => {},
        }
        self.space.change = Change::None;
    }

    pub fn load_prev(&mut self) {
        match self.history.prev() {
            Save::Current(c) => self.space.current = c,
            Save::Space(s) => self.space = s,
            Save::Size(e, s) => self.space.edge_size_no_change(e, s),
        }
    }

    pub fn load_next(&mut self) {
        match self.history.next() {
            Save::Current(c) => self.space.current = c,
            Save::Space(s) => self.space = s,
            Save::Size(e, s) => self.space.edge_size_no_change(e, s),
        }
    }

    pub fn load_prev_if_any(&mut self) {
        if self.history.has_prev() {
            self.load_prev();
        }
    }

    pub fn load_next_if_any(&mut self) {
        if self.history.has_next() {
            self.load_next();
        }
    }
}
