#[macro_use] extern crate conrod;
extern crate find_folder;
extern crate piston_window;
extern crate graphics;
extern crate rand;
extern crate input;
extern crate image;
extern crate gfx_device_gl;


mod data;
use data::*;

mod display;
use display::*;

use conrod::Theme;
use conrod::color;
use conrod::events::input_provider::InputProvider;
use piston_window::{
    Glyphs, OpenGL, PistonWindow, WindowSettings, UpdateEvent, EventLoop,
    Window, ImageSize
};
use input::keyboard::Key;
use graphics::{Transformed, Image, DrawState};

fn handle_keys(ui: &Ui, d: &mut Data) {
    let mut keys = ui.global_input.keys_just_pressed().collect::<Vec<_>>();
    keys.dedup();
    for k in keys {
        match k {
            Key::R => d.space.rotate(),
            Key::E => d.space.set_pos_kind(PosKind::Edge),
            Key::F => d.space.set_pos_kind(PosKind::Face),
            Key::N => d.load_next_if_any(),
            Key::P => d.load_prev_if_any(),
            _ => match d.space.current {
                Pos::E((_, a)) => match k {
                    Key::PageUp => d.space.inc_size(),
                    Key::PageDown => d.space.dec_size(),
                    _ => match a {
                        Axis::X => match k {
                            Key::Right => d.space.inc_pos(),
                            Key::Left => d.space.dec_pos(),
                            _ => {},
                        },
                        Axis::Y => match k {
                            Key::Up => d.space.dec_pos(),
                            Key::Down => d.space.inc_pos(),
                            _ => {},
                        },
                        Axis::Z => match k {
                            Key::Up => d.space.dec_pos(),
                            Key::Down => d.space.inc_pos(),
                            _ => {},
                        },
                    },
                },
                Pos::F((_, o)) => match k {
                        Key::A => d.space.set_current_face_if_none(),
                        _ => match o {
                            Ori::Top => match k {
                                Key::Up => d.space.inc_pos2(),
                                Key::Down => d.space.dec_pos2(),
                                Key::Left => d.space.dec_pos3(),
                                Key::Right => d.space.inc_pos3(),
                                _ => {},
                            },
                            Ori::Back => match k {
                                Key::Left => d.space.dec_pos2(),
                                Key::Right => d.space.inc_pos2(),
                                Key::Up => d.space.dec_pos3(),
                                Key::Down => d.space.inc_pos3(),
                                _ => {},
                            },
                            Ori::Side => match k {
                                Key::Up => d.space.dec_pos2(),
                                Key::Down => d.space.inc_pos2(),
                                Key::Left => d.space.dec_pos3(),
                                Key::Right => d.space.inc_pos3(),
                                _ => {},
                        },
                    },
                },
            },
        };
    }
}

fn main() {
    let opengl = OpenGL::V3_2;
    let mut window: PistonWindow = WindowSettings::new("LP74", [900, 600])
        .opengl(opengl).exit_on_esc(true).build().unwrap();

	let mut ui = {
        let assets = find_folder::Search::KidsThenParents(3, 5)
            .for_folder("assets").expect("Cannot find assets folder");
        let font_path = assets.join("fonts/NotoSans/NotoSans-Regular.ttf");
        let theme = Theme::default();
        let glyph_cache = Glyphs::new(&font_path, window.factory.clone());
        Ui::new(glyph_cache.unwrap(), theme)
    };

    window.set_ups(30);
    window.set_max_fps(30);

    let mut colors = Colors {
        c1: color::rgb_bytes(57, 63, 63),
        c2: color::rgb_bytes(30, 34, 34),
        font: color::WHITE,
        hide: color::rgba_bytes(57, 63, 63, 0.4),
    };

    let mut state = State::Design;
    let mut data = Data::new();

    while let Some(event) = window.next() {
        let wsize = WSize {
            header_h: 50.0,
            side_w: 150.0,
            w: window.size().width as f64,
            h: window.size().height as f64
        };

        ui.handle_event(&event);
        event.update(|_| ui.set_widgets(|ref mut uicell|
                                        set_ui(uicell,
                                               &mut state,
                                               &mut data,
                                               &mut colors,
                                               &wsize)));
        state = match state {
            State::Visual(faces_map) => match faces_map {
                None => match generate_faces_map(&mut window, &data.space) {
                        None => State::Err(String::from("Graphviz project's \"dot\" is not installed")),
                        fmap_opt => State::Visual(fmap_opt),
                },
                Some(_) => State::Visual(faces_map),
            },
            State::Design => {
                handle_keys(&ui, &mut data);
                State::Design
            },
            State::Extensions => State::Err(String::from("No available extensions")),
            s => s,
        };
        window.draw_2d(&event, |c, g| {
            match &state {
                &State::Design => {
                    if data.space.change != Change::None
                        || data.history.change
                        || ui.will_redraw() {
                        data.history.change = false;
                        piston_window::clear([0.0, 0.0, 0.0, 0.0], g);
                        show_space(&data.space, &wsize, c, g);
                        ui.needs_redraw();
                    }
                    data.save_if_changed();
                },
                &State::Visual(ref faces_map) => {
                    if ui.will_redraw() {
                        if let &Some(ref m) = faces_map {
                            piston_window::clear([1.0, 1.0, 1.0, 1.0], g);
                            let image = Image::new()
                                .rect(final_size(wsize,
                                                 (m.get_width() as f64,
                                                  (m.get_height()) as f64)));
                            image.draw(m, &DrawState::default(),
                                       c.transform.trans(0.0, 0.0), g);
                            ui.needs_redraw();
                        }
                    }
                },
                &State::Extensions => {},
                &State::Err(_) => {},
            }
            ui.draw_if_changed(c, g);
        });
    }
}
