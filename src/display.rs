extern crate conrod;
extern crate find_folder;
extern crate piston_window;
extern crate graphics;
extern crate rand;
extern crate input;
extern crate image;
extern crate gfx_device_gl;

use data::*;

use graphics::{Line, Rectangle, Polygon, Ellipse, DrawState, Transformed};
use conrod::{
    Color, Button, Colorable, Labelable, Frameable, Positionable,
    Sizeable, Graphics, Canvas, Widget, Text, DButton, Slider, DropDownList
};
use conrod::color;
use piston_window::{
    Glyphs, PistonWindow,
    Window, Texture
};
use std::process::{Command, Stdio};
use image::{ImageFormat, load_from_memory_with_format};
use std::io::Write;
use std::io::Read;


pub type Backend = (
    <piston_window::G2d<'static> as Graphics>::Texture,
    Glyphs
);
pub type Ui = conrod::Ui<Backend>;
pub type UiCell<'a> = conrod::UiCell<'a, Backend>;

const BUTTON_WIDTH: f64 = 110.0;
const BUTTON_HEIGHT: f64 = 35.0;

const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
const RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];

pub struct WSize {
    pub w: f64,
    pub h: f64,
    pub header_h: f64,
    pub side_w: f64,
}

widget_ids! {
    C_MASTER,
    C_HEADER,
    C_SIDE,
    BTN_ADD,
    DBTN_ADD,
    BTN_DEL,
    DBTN_DEL,
    BTN_PREV,
    DBTN_PREV,
    BTN_NEXT,
    DBTN_NEXT,
    C_ERR,
    TXT_ERR,
    BTN_OK_ERR,
    BTN_CANCEL_ERR,
    BTN_DESIGN,
    DBTN_DESIGN,
    BTN_VISUAL,
    DBTN_VISUAL,
	COLOR_SLIDER with 3,
	DCOLOR_SLIDER with 3,
    SIZE_SLIDER,
    POS_KIND_SELECT,
    BTN_ROTATE,
	COLOR_SELECT,
    DBTN_SELECT_COLOR,
    LENGTH_TXT,
    BTN_EXTENSIONS,
    //DBTN_EXTENSIONS,
}

pub struct Colors {
    pub c1: Color,
    pub c2: Color,
    pub font: Color,
    pub hide: Color,
}

pub enum State {
    Design,
    Visual(Option<Texture<gfx_device_gl::Resources>>),
    Extensions,
    Err(String),
}

fn make_button<'a, F>(c: &Colors) -> Button<'a, F>
    where F: FnOnce()
{
    Button::new().w_h(BUTTON_WIDTH, BUTTON_HEIGHT).color(c.c1)
        .label_color(c.font).frame_color(c.c2)
}

macro_rules! make_disable_button {
    ($c:ident) => {
        DButton::new()
            .color($c.c1).w_h(BUTTON_WIDTH, BUTTON_HEIGHT).frame_color($c.c2)
    }
}

pub fn generate_faces_map(w: &mut PistonWindow,s: &Space) ->
    Option<Texture<gfx_device_gl::Resources>>
{
    let wsize = w.size();
    let process = match Command::new("dot")
        .arg("-Tpng")
        .arg(format!("-Gsize={},{}!", wsize.width, wsize.height - 50))
        .arg("-Gdpi=2")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn() {
        Ok(p) => p,
        Err(_) => return None,
    };
    process.stdin.unwrap().write_all(s.dot_graph_code()
                                     .as_bytes())
        .unwrap_or_else(|e| panic!("piping failed: {}", e));
    let mut png = Vec::new();
    process.stdout.unwrap().read_to_end(&mut png)
        .unwrap_or_else(|_| panic!("final read failed"));
    let rgba_img = load_from_memory_with_format(&png,
                                                ImageFormat::PNG)
        .unwrap_or_else(|e| panic!("PNG image loading failed: {}", e))
        .to_rgba();
    let factory = &mut w.factory;
    let settings = piston_window::TextureSettings::new();
    Some(Texture::from_image(factory, &rgba_img, &settings)
             .unwrap())
}

pub fn final_size(WSize { header_h: hh, side_w: _, w: ww, h: wh_ }: WSize,
              (iw, ih): (f64, f64)) -> [f64; 4]
{
    let wh = wh_ - hh;
    let wr = ww / wh;
    let ir = iw / ih;
    if wr > ir {
        let w = iw * (wh / ih);
        [(ww / 2.0) - (w / 2.0), hh, w, wh]
    } else {
        let h = ih * (ww / iw);
        [0.0, (wh / 2.0) - (h / 2.0) + hh, ww, h]
    }
}

macro_rules! line {
    ($color:expr, $c:expr, $gl:expr, ($x:expr, $z:expr),
    ($ax:expr, $az:expr), ($bx:expr, $bz:expr)) => {
        Line::new($color, 1.0)
            .draw([$x + $ax, $z + $az,
                  $x + $bx, $z + $bz],
                  &DrawState::default(),
                  $c.transform,
                  $gl)
    }
}

macro_rules! black_line {
    ($c:expr, $gl:expr, ($x:expr, $z:expr),
    ($ax:expr, $az:expr), ($bx:expr, $bz:expr)) => {
        line!(BLACK, $c, $gl, ($x, $z), ($ax, $az), ($bx, $bz))
    }
}

macro_rules! red_line {
    ($c:expr, $gl:expr, ($x:expr, $z:expr),
    ($ax:expr, $az:expr), ($bx:expr, $bz:expr)) => {
        line!(RED, $c, $gl, ($x, $z), ($ax, $az), ($bx, $bz))
    }
}

pub fn show_space<G: graphics::Graphics>(s: &Space,
                 wsize: &WSize,
                 c: graphics::Context, gl: &mut G) {
    let (xctr, zctr) = ((wsize.w - wsize.side_w) / 2.0, (wsize.h - wsize.header_h) / 2.0);
    let (xcur, ycur, zcur) = (s.row_size_before_current(Axis::X) as f64,
                              (s.row_size_before_current(Axis::Y) / 3) as f64,
                              s.row_size_before_current(Axis::Z) as f64);
    let (wshift, hshift) = (xctr - (xcur + ycur) + wsize.side_w,
                            zctr - (zcur + ycur) + wsize.header_h);
    let mut wy = (s.row_size(Axis::Y) / 3) as f64;
    for iy in (0..s.faces[0].len()).rev() {
        let ysize = (s.size[Space::axis_idx(Axis::Y)][iy] / 3) as f64;
        let mut wx = wshift;
        for ix in 0..s.faces.len() {
            let xsize = s.size[Space::axis_idx(Axis::X)][ix] as f64;
            let mut wz = hshift;
            for iz in 0..s.faces[0][0].len() {
                let zsize = s.size[Space::axis_idx(Axis::Z)][iz] as f64;
                match s.faces[ix][iy][iz] {
                    Some(ss) => {
                        let x = wx + wy;
                        let z = wz + wy;
                        if let Some((r, g, b)) = ss[Space::ori_idx(Ori::Back)] {
                            Rectangle::new([r, g, b, 0.6])
                                .draw([0.0, 0.0, xsize, zsize],
                                      &DrawState::default(),
                                      c.transform.trans(x, z),
                                      gl);
                            Rectangle::new_border(BLACK, 1.0)
                                .draw([0.0, 0.0, xsize, zsize],
                                      &DrawState::default(),
                                      c.transform.trans(x, z),
                                      gl);
                        }
                        if let Some((r, g, b)) = ss[Space::ori_idx(Ori::Top)] {
                            Polygon::new([r, g, b, 0.6])
                                .draw(&[[x, z],
                                      [x + xsize, z],
                                      [x + xsize - ysize, z - ysize],
                                      [x - ysize, z - ysize]],
                                      &DrawState::default(),
                                      c.transform,
                                      gl);
                            black_line!(c, gl, (x, z), (0.0, 0.0), (xsize, 0.0));
                            black_line!(c, gl, (x, z), (xsize, 0.0), (xsize - ysize, -ysize));
                            black_line!(c, gl, (x, z), (xsize - ysize, -ysize), (-ysize, -ysize));
                            black_line!(c, gl, (x, z), (-ysize, -ysize), (0.0, 0.0));
                        }
                        if let Some((r, g, b)) = ss[Space::ori_idx(Ori::Side)] {
                            Polygon::new([r, g, b, 0.6])
                                .draw(&[[x, z + zsize],
                                      [x, z],
                                      [x - ysize, z - ysize],
                                      [x - ysize, z + zsize - ysize]],
                                      &DrawState::default(),
                                      c.transform,
                                      gl);
                            black_line!(c, gl, (x, z), (0.0, zsize), (0.0, 0.0));
                            black_line!(c, gl, (x, z), (0.0, 0.0), (-ysize, -ysize));
                            black_line!(c, gl, (x, z), (-ysize, -ysize), (-ysize, zsize - ysize));
                            black_line!(c, gl, (x, z), (-ysize, zsize - ysize), (0.0, zsize));
                        }
                        if (ix, iy, iz) == s.current_node_coord() {
                            Ellipse::new(RED)
                                .draw([-3.0, -3.0, 7.0, 7.0],
                                      &DrawState::default(),
                                      c.transform.trans(x, z),
                                      gl);
                            match s.current {
                                Pos::E((_, a)) => match a {
                                    Axis::X => Line::new(RED, 1.0)
                                        .draw([x, z, x + xsize, z],
                                              &DrawState::default(),
                                              c.transform,
                                              gl),
                                    Axis::Y => Line::new(RED, 1.0)
                                        .draw([x, z, x - ysize, z - ysize],
                                              &DrawState::default(),
                                              c.transform,
                                              gl),
                                    Axis::Z => Line::new(RED, 1.0)
                                        .draw([x, z, x, z + zsize],
                                              &DrawState::default(),
                                              c.transform,
                                              gl),
                                },
                                Pos::F((_, o)) => match o {
                                    Ori::Back => Rectangle::new_border(RED, 1.0)
                                        .draw([0.0, 0.0, xsize, zsize],
                                              &DrawState::default(),
                                              c.transform.trans(x, z),
                                              gl),
                                    Ori::Top => {
                                        red_line!(c, gl, (x, z), (0.0, 0.0), (xsize, 0.0));
                                        red_line!(c, gl, (x, z), (xsize, 0.0), (xsize - ysize, -ysize));
                                        red_line!(c, gl, (x, z), (xsize - ysize, -ysize), (-ysize, -ysize));
                                        red_line!(c, gl, (x, z), (-ysize, -ysize), (0.0, 0.0));
                                    },
                                    Ori::Side => {
                                        red_line!(c, gl, (x, z), (0.0, zsize), (0.0, 0.0));
                                        red_line!(c, gl, (x, z), (0.0, 0.0), (-ysize, -ysize));
                                        red_line!(c, gl, (x, z), (-ysize, -ysize), (-ysize, zsize - ysize));
                                        red_line!(c, gl, (x, z), (-ysize, zsize - ysize), (0.0, zsize));
                                    },
                                },
                            }
                        }
                    },
                    None => {},
                }
                wz += zsize;
            }
            wx += xsize;
        }
        wy -= ysize;
    }
}

pub fn set_ui(ui: &mut UiCell, s: &mut State,
          d: &mut Data, c: &mut Colors, wsize: &WSize)
{
    match s {
        &mut State::Design => {
            header_ui(ui, c, wsize);
            side_ui(ui, c, wsize);
            std_ui(ui, s, d, c);
        },
        &mut State::Visual(_) => {
            header_ui(ui, c, wsize);
            visual_ui(ui, s, c);
        },
        &mut State::Extensions => /*extensions_ui(ui, s, c)*/ {},
        &mut State::Err(_) => error_ui(ui, s, c),
    }
}

fn error_ui(ui: &mut UiCell, gs: &mut State, c: &mut Colors)
{
    {
        Canvas::new()
            .color(c.hide)
            .set(C_MASTER, ui);
        Canvas::new().middle_of(C_MASTER)
            .color(c.c1).frame_color(c.c2)
            .w_h(400.0, 120.0).pad(20.0)
            .set(C_ERR, ui);
        Text::new(match *gs {
            State::Err(ref msg) => msg,
            _=> "",
        })
            .color(c.font).mid_top_of(C_ERR)
            .set(TXT_ERR, ui);
    }
    make_button(c)
        .label("OK").mid_bottom_of(C_ERR)
        .react(|| {
            *gs = State::Design;
        })
        .set(BTN_OK_ERR, ui);
}

fn header_ui(ui: &mut UiCell, c: &mut Colors, wsize: &WSize)
{
    Canvas::new()
        .color(c.c1).frame_color(c.c2)
        .y((wsize.h/2.0)-(wsize.header_h/2.0)).h(wsize.header_h)
        .pad_left(20.0).pad_right(20.0)
        .set(C_HEADER, ui);
}

fn side_ui(ui: &mut UiCell, c: &mut Colors, wsize: &WSize)
{
    Canvas::new().color(c.c1)
        .color(c.c1).frame_color(c.c2)
        .x_y(-((wsize.w/2.0)-(wsize.side_w/2.0)), -(wsize.header_h/2.0))
        .w_h(wsize.side_w, wsize.h - wsize.header_h)
        .pad(20.0)
        .set(C_SIDE, ui);
}

fn visual_ui(ui: &mut UiCell, gs: &mut State, c: &mut Colors)
{
    make_button(c) 
        .mid_right_of(C_HEADER)
        .label("Extensions").mid_right_of(C_HEADER)
        .react(|| {
            *gs = State::Extensions;
        })
        .set(BTN_EXTENSIONS, ui);
    make_disable_button!(c)
        .mid_right_of(C_HEADER)
        .left_from(BTN_EXTENSIONS, 0.0)
        .label("Visual")
        .set(DBTN_VISUAL, ui);
    make_button(c) 
        .mid_right_of(C_HEADER)
        .label("Design").left_from(DBTN_VISUAL, 0.0)
        .react(|| {
            *gs = State::Design;
        })
        .set(BTN_DESIGN, ui);
}
fn std_ui(ui: &mut UiCell, gs: &mut State, d: &mut Data, c: &mut Colors)
{
    let prev_id = if d.history.has_prev() {
        Button::new()
            .mid_left_of(C_HEADER).w_h(50.0, 30.0)
            .color(c.c1).frame_color(c.c2)
            .label_color(c.font)
            .label("←").react(|| {
                d.load_prev();
            })
            .label_font_size(32)
            .set(BTN_PREV, ui);
        BTN_PREV
    } else {
        make_disable_button!(c)
            .label("←")
            .mid_left_of(C_HEADER).w_h(50.0, 30.0)
            .label_font_size(32)
            .set(DBTN_PREV, ui);
        DBTN_PREV
    };
    let next_id = if d.history.has_next() {
        Button::new()
            .mid_left_of(C_HEADER).right_from(prev_id, 30.0)
            .w_h(50.0, 30.0)
            .color(c.c1).frame_color(c.c2)
            .label_color(c.font)
            .label("→").react(|| {
                d.load_next();
            })
            .label_font_size(32)
            .set(BTN_NEXT, ui);
        BTN_NEXT
    } else {
        make_disable_button!(c)
            .label("→")
            .mid_left_of(C_HEADER).right_from(prev_id, 30.0)
            .w_h(50.0, 30.0)
            .label_font_size(32)
            .set(DBTN_NEXT, ui);
        DBTN_NEXT
    };
    {
        let mut select = vec![String::from("Edge (E)"),
                              String::from("Face (F)")];
        let mut select_idx = None;
	    DropDownList::new(&mut select, &mut select_idx)
            .w_h(BUTTON_WIDTH, BUTTON_HEIGHT)
	    	.mid_left_of(C_HEADER).right_from(next_id, 30.0)
            .max_visible_items(2)
            .color(c.c1)
            .frame_color(c.c2)
            .label("Select")
            .label_color(c.font)
            .react(|selected_idx: &mut Option<usize>, new_idx, string: &str| {
                *selected_idx = Some(new_idx);
                d.space.set_pos_kind(match string {
                    "Edge (E)" => PosKind::Edge,
                    _ => PosKind::Face,
                });
            })
            .set(POS_KIND_SELECT, ui);
    }
    make_button(c)
        .mid_left_of(C_HEADER).right_from(POS_KIND_SELECT, 30.0)
        .label("Rotate (R)")
        .react(|| {
            d.space.rotate();
        }).set(BTN_ROTATE, ui);
    make_button(c)
        .mid_right_of(C_HEADER)
        .label("Extensions")
        .react(|| {
            *gs = State::Extensions;
        }).set(BTN_EXTENSIONS, ui);
    make_button(c)
        .mid_right_of(C_HEADER)
        .left_from(BTN_EXTENSIONS, 0.0)
        .label("Visual")
        .react(|| {
            *gs = State::Visual(None);
        }).set(BTN_VISUAL, ui);
    make_disable_button!(c) 
        .mid_right_of(C_HEADER)
        .label("Design").left_from(BTN_VISUAL, 0.0)
        .set(DBTN_DESIGN, ui);
    match d.space.current {
        Pos::F(((x, y, z), o)) => {
            if let Some(n) = d.space.faces[x][y][z] {
	            if let Some((r, g, b)) = n[Space::ori_idx(o)] {
                    make_disable_button!(c)
                        .mid_top_of(C_SIDE)
                        .label("Add (A)")
                        .set(DBTN_ADD, ui);
                    make_button(c)
                        .down(20.0).label("Delete (D)")
                        .react(|| {
                            d.space.remove_face(((x, y, z), o));
                        }).set(BTN_ADD, ui);
    	            for i in 0..3 {
    	                let color = match i {
    	                    0 => color::rgb(r, 0.0, 0.0),
    	                    1 => color::rgb(0.0, g, 0.0),
    	                    _ => color::rgb(0.0, 0.0, b),
    	                };
    	            
    	                let value = match i {
    	                    0 => r,
    	                    1 => g,
    	                    _ => b,
    	                };
    	            
    	                Slider::new(value, 0.0, 1.0)
    	                    .and(|slider| if i == 0 { slider.down(40.0) } else { slider.right(10.0) })
    	                    .w_h(30.0, 80.0)
    	                    .color(color)
    	                    .label(&format!("{:.*}", 2, value))
    	                    .label_color(color.plain_contrast())
    	                    .frame_color(c.c2)
    	                    .react(|v| d.space.set_current_face_color_if_any(match i {
    	                        0 => (v, g, b),
    	                        1 => (r, v, b),
    	                        _ => (r, g, v),
    	                    }))
    	                    .set(COLOR_SLIDER + i, ui);
		            }
                    {
                        let color = color::rgb(r, g, b);
						let mut colors = vec![
                            "Black".to_string(),
                            "Gray".to_string(),
                            "White".to_string(),
                            "Brown".to_string(),
                            "Red".to_string(),
                            "Orange".to_string(),
                            "Yellow".to_string(),
                            "Green".to_string(),
                            "Blue".to_string(),
                            "Purple".to_string()
                        ];
                        let mut selected_idx = None;
						DropDownList::new(&mut colors, &mut selected_idx)
						    .w_h(BUTTON_WIDTH, BUTTON_HEIGHT)
                            .mid_top_of(C_SIDE).down(20.0)
						    .color(color)
						    .frame_color(c.c2)
						    .label("Colors")
						    .label_color(color.plain_contrast())
						    .react(|selected_idx: &mut Option<usize>, new_idx, string: &str| {
						        *selected_idx = Some(new_idx);
						        d.space.set_current_face_color_if_any(match string {
						            "Black" => (0.0, 0.0, 0.0),
						            "White" => (1.0, 1.0, 1.0),
						            "Red"   => (1.0, 0.0, 0.0),
						            "Green" => (0.0, 1.0, 0.0),
						            "Blue"  => (0.0, 0.0, 1.0),
                                    "Yellow" => (color::YELLOW.red(),
                                                 color::YELLOW.green(),
                                                 color::YELLOW.blue()),
                                    "Purple" => (color::PURPLE.red(),
                                                 color::PURPLE.green(),
                                                 color::PURPLE.blue()),
                                    "Orange" => (color::ORANGE.red(),
                                                 color::ORANGE.green(),
                                                 color::ORANGE.blue()),
                                    "Gray" => (color::GRAY.red(),
                                               color::GRAY.green(),
                                               color::GRAY.blue()),
                                    "Brown" => (color::BROWN.red(),
                                                color::BROWN.green(),
                                                color::BROWN.blue()),
						            _       => (1.0, 1.0, 1.0),
						        });
						    })
						    .set(COLOR_SELECT, ui);
                    }
                } else {
                    make_button(c)
                        .mid_top_of(C_SIDE).label("Add (A)")
                        .react(|| {
                            d.space.add_face(((x, y, z), o));
                        }).set(BTN_ADD, ui);
                    make_disable_button!(c)
                        .down(20.0).label("Delete (D)")
                        .set(DBTN_DEL, ui);
    	            for i in 0..3 {
    	                Slider::new(0.0, 0.0, 1.0)
    	                    .and(|slider| if i == 0 {
                                slider.down(40.0)
                            } else {
                                slider.right(10.0)
                            })
    	                    .w_h(30.0, 80.0)
    	                    .color(c.c1)
    	                    .label_color(c.font)
    	                    .frame_color(c.c2)
    	                    .react(|_| {})
    	                    .set(DCOLOR_SLIDER + i, ui);
                    }
                    make_disable_button!(c)
                        .mid_top_of(C_SIDE).down(20.0)
                        .label("Color")
                        .set(DBTN_SELECT_COLOR, ui);
                }
            }
        },
        Pos::E(e) => {
            Text::new("Length")
                .top_left_of(C_SIDE)
                .down(115.0)
                .font_size(20)
                .color(c.font)
                .set(LENGTH_TXT, ui);
            let length = d.space.get_edge_size(e) as f64;
    	    Slider::new(length, 1.0, 1024.0)
    	        .top_right_of(C_SIDE)
    	        .w_h(30.0, 200.0)
    	        .color(c.c1)
    	        .label(&format!("{:.*}", 0, length))
    	        .label_color(c.font)
    	        .frame_color(c.c2)
    	        .react(|v| d.space.edge_size(e, v as usize))
    	        .set(SIZE_SLIDER, ui);
        },
	}
}
